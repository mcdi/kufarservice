import telebot
import json


def logic(message_handler):
    """ General logic for example """

    # Not required mark. For IDE only
    from ...message import MessageHandler
    message_handler: MessageHandler
    ###

    if message_handler.message.text == "/start":
        message_handler.bot.send_message(message_handler.message.chat.id, "Hello World")
        return

    if message_handler.call:
        print(message_handler.call.data)
        return

