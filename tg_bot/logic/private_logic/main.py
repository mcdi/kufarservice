from .kufar.main import logic as kufar_logic
from .general.main import logic as general_logic


def get_logic_by_label():
    return kufar_logic