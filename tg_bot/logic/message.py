import telebot
from ..models import BotUser, Bot
from .private_logic.main import get_logic_by_label


class MessageHandler:

	""" Extend for basic telebot message, which contains our logic """

	def __init__(self,
				 bot: telebot.TeleBot,
				 telebot_message: telebot.types.Message=None,
				 telebot_call: telebot.types.CallbackQuery=None):

		self.bot = bot
		self.call = telebot_call
		self.message = telebot_message

		self.user_model = self._user_model if self.message else None
		self.bot_model = None


	@property
	def _user_model(self) -> BotUser:
		_bot_model = Bot.objects.get(token=self.bot.token)
		self.bot_model = _bot_model
		user = BotUser.objects.get_or_create(
			bot=_bot_model,
			chat_id=self.message.chat.id,
			defaults={
				'name': self.message.chat.first_name,
				'phone_number': "-"})
		return user[0]


	def process_with_logic(self):
		logic = get_logic_by_label()
		try:
			logic(self)
		except Exception as e:
			print(e)
