from django.db import models
from .user import BotUser
import requests


class KufarService(models.Model):
    host = models.CharField(max_length=1000)
    port = models.CharField(max_length=50)
    route = models.CharField(max_length=1000)

    def set_webhook(self):
        pass


class KufarQuery(models.Model):
    user = models.ForeignKey(BotUser, on_delete=models.CASCADE)
    service = models.ForeignKey(KufarService, on_delete=models.CASCADE)


