from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('tg_bot/', include("tg_bot.urls")),
    path('kufar/', include("kufar.urls")),
    path('admin/', admin.site.urls),
]
