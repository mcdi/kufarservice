from typing import List

class Param:
    param_name_ru = ""
    value_text = ""
    param_name_en = ""
    value_id = ""

    def load_from_kufar_json(self, js):
        self.param_name_ru = js["pl"]
        self.value_text = js["vl"]
        self.param_name_en = js["p"]
        self.value_id = js["v"]
        return self


class Product:
    """ Instance of Kufar Product"""
    product_id = 0
    name = ""
    category = ""
    price_byn = ""
    price_usd = ""
    url = ""
    parameters = []
    account_id = 0
    company = False
    currency = "BYR"
    images = []
    list_time = ""
    halva = False
    phone = ""


    def load_from_kufar_json(self, js):
        self.account_id =  js["account_id"]
        self.product_id =  js["ad_id"]
        self.parameters: List[Param] = js["ad_parameters"]
        self.url = js["ad_link"]
        self.category = js["category"]
        self.company = js["company_ad"]
        self.currency = js["currency"]
        self.images = js["images"]
        self.pub_time = js["list_time"]
        self.halva = js["paid_services"]["halva"]
        try:
         self.phone = js["phone"]
        except:
            self.phone = ""
        self.price_byn = round(float(js["price_byn"])/100, 2)
        self.price_usd =round(float(js["price_usd"])/100, 2)
        self.name = js["subject"]

        self.transform_images()
        self.transform_params_to_obj()

        return self

    def transform_images(self):
        new_images = []
        for image in self.images:
            id = image["id"]
            url = "https://yams.kufar.by/api/v1/kufar-ads/images/05/{}.jpg?rule=gallery".format(id)
            new_images.append(url)
        self.images = new_images

    def transform_params_to_obj(self):
        new_params = []
        for param in self.parameters:
            new_param = Param().load_from_kufar_json(param)
            new_params.append(new_param)
        self.parameters = new_params