from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import json

@csrf_exempt
def create_new_search_query(request):
	"""
	Добавляет новую строку поиска
	Формат данных:
	{"host": "http://***", "port":"1234", "route": "/sdfs/sdf", "query": "MacBook Pro"}
	"""
	data = json.loads(request.body.decode())
	print(data)
	return JsonResponse({"query_id": 1})


@csrf_exempt
def remove_search_query(request):
	"""
	Удаляет строку поиска
	Формат данных:
	{"id": "1221"}
	 """
	return JsonResponse({"query_id": 1})



"""  
@csrf_exempt
def get_products_by_query_id(request):
	return JsonResponse({"products": []})
	
"""