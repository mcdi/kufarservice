from django.db import models
import requests


class WebhookClient(models.Model):
	webhook_host = models.CharField(max_length=1000)
	extended_url = models.CharField(max_length=1000)
	webhook_port = models.IntegerField()


	@property
	def webhook_url(self):
		url = f"{self.webhook_host}:{self.webhook_port}{self.extended_url}"
		return url


	def send_message(self, message_dict):
		response = requests.post(url=self.webhook_url, json=message_dict)
		return response