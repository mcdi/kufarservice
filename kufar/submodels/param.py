from django.db import models
from .product import KufarProduct


class KufarProductParam(models.Model):
	product = models.ForeignKey(KufarProduct, on_delete=models.CASCADE)

	name_ru = models.CharField(max_length=1000)
	value_text = models.CharField(max_length=1000)
	name_en  = models.CharField(max_length=1000)
	value_id  = models.CharField(max_length=1000)
