from django.db import models
from ..kufar_api.search import Search
from .webhook import WebhookClient


class KufarSearchRequest(models.Model):
	webhook_client = models.ForeignKey(WebhookClient, on_delete=models.SET_NULL, null=True)
	query = models.CharField(max_length=1000)

	def __str__(self):
		return self.query

	@property
	def api(self) -> Search:
		api = Search(self.query)
		return api

	def refresh_products(self):
		"""
		Проверить есть ли такие товары.
		Если каких-то нет, записать товар и отправить вебхук.
		В тех, что есть - проверить изменилась ли цена.
		Если изменилась - перезаписать и отправить вебхук.
		"""
		api_products = self.api.products()

		product_ids = [product.product_id for product in api_products]
		exists_products = self.kufarproduct_set.filter(product_id__in=product_ids, search_request=self)
		exists_product_ids = list(exists_products.values_list("product_id", flat=True))

		exists_api_products = list(filter(lambda product: product.product_id in exists_product_ids, api_products)) # Этих на проверку изменений.
		not_exists_api_products = list(filter(lambda product: product.product_id not in exists_product_ids, api_products)) # Этих на сохранение.

		exists_products_json = self.__exists_products_handler(exists_api_products, exists_products)
		not_exists_products_json = self.__not_exists_products_handler(not_exists_api_products)

		data =  {
			"exists_products": exists_products_json,
			"new_products": not_exists_products_json
		}
		if len(exists_products_json) > 0 or len(not_exists_products_json) > 0:
			self.send_webhook(data)
		return data

	def __exists_products_handler(self, api_products, exists_models):
		changed_products = []
		for product in exists_models:
			api_product_filter = list(filter(lambda prod:prod.product_id == product.product_id, api_products))
			if len(api_product_filter) == 0:
				continue
			api_product = api_product_filter[0]
			if product.price_byn != api_product.price_byn:
				product.old_price = product.price_byn
				product.fill_from_api_product(api_product)
				product.save()
				changed_products.append(product)
		return self.transform_products_to_json(changed_products)

	def __not_exists_products_handler(self, api_products):
		new_products_list = []
		for api_product in api_products:
			new_product = self.kufarproduct_set.create(search_request=self)
			new_product.fill_from_api_product(api_product=api_product)
			new_product.save()
			new_products_list.append(new_product)
			params = new_product.get_params(api_params=api_product.parameters)
			for param in params:
				param.save()
		return self.transform_products_to_json(new_products_list)

	def transform_products_to_json(self, products):
		dataset = []
		for product in products:
			dataset.append(product.webhook_view)
		return dataset

	def send_webhook(self, json_data):
		response = self.webhook_client.send_message(json_data)
		return response