# Generated by Django 3.0.6 on 2020-09-24 21:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='KufarProduct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_id', models.IntegerField(default=0)),
                ('account_id', models.IntegerField(default=0)),
                ('name', models.CharField(blank=True, max_length=1000, null=True)),
                ('category', models.CharField(blank=True, max_length=1000, null=True)),
                ('price_byn', models.FloatField(blank=True, max_length=1000, null=True)),
                ('price_usd', models.FloatField(blank=True, max_length=1000, null=True)),
                ('url', models.CharField(blank=True, max_length=1000, null=True)),
                ('images', models.CharField(default='[]', max_length=1000)),
                ('list_time', models.CharField(blank=True, max_length=1000, null=True)),
                ('halva', models.BooleanField(blank=True, null=True)),
                ('company', models.BooleanField(blank=True, null=True)),
                ('phone', models.CharField(max_length=1000)),
                ('currency', models.CharField(default='BYR', max_length=1000)),
                ('old_price', models.FloatField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='WebhookClient',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('webhook_host', models.CharField(max_length=1000)),
                ('extended_url', models.CharField(max_length=1000)),
                ('webhook_port', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='KufarSearchRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('query', models.CharField(max_length=1000)),
                ('webhook_client', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='kufar.WebhookClient')),
            ],
        ),
        migrations.CreateModel(
            name='KufarProductParam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_ru', models.CharField(max_length=1000)),
                ('value_text', models.CharField(max_length=1000)),
                ('name_en', models.CharField(max_length=1000)),
                ('value_id', models.CharField(max_length=1000)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kufar.KufarProduct')),
            ],
        ),
        migrations.AddField(
            model_name='kufarproduct',
            name='search_request',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kufar.KufarSearchRequest'),
        ),
    ]
